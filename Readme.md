# Leed RSS docker

## What is this

This is a docker deployment for [leed rss reader](https://github.com/LeedRSS)

## Requirements

+ [Docker](https://docker.com)
+ [Docker-compose](https://docs.docker.com/compose/install/)

## Installation

```
# 1. Clone this repository
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/beniamid/leed-rss-docker
# 2. Move to the cloned directory
cd leed-rss-docker
# 3. Copy the default conig
cp .env.sample .env
# 4. edit .env and adapt to your nees (see comments in .env file)
$EDITOR .env
# 5. launch the application
docker-compose up
# 6 Wait about 10 seconds to see lines like these and follow the link shown
leed_1        | 	You can now got to http://localhost:8088
leed_1        | 	Default user / password is admin / admin
leed_1        | 	to change the password go to http://localhost:8090/settings.php#preferenceBloc
```

### Developpment consideration

In developpment mode, xdebug is enabled to make it work you need :

1. To send the xdebug cookie on your requests, for instance with [this firefox plugin](https://addons.mozilla.org/en-US/firefox/addon/xdebug-ext-quantum/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search)
2. To tell your browser to listen on port `9000`

## TODO

+ test prod
