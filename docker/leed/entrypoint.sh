#!/bin/bash

perms(){
    echo "Fixing permissions"
    chown -R :www-data .
    chmod -R g+w .
}


info(){
    if [ ! -z "$LEED_PORT" ]; then
        url=http://localhost:$LEED_PORT
    else
        url="http://$(hostname -i)"
    fi
    echo -e "\t\033[0;32mYou can now got to $url"
    echo -e "\t\033[0;33mDefault user / password is $INITIAL_USER / $INITIAL_PASSWORD"
    echo -e "\t\033[0;33mto change the password go to http://localhost:$LEED_PORT/settings.php#preferenceBloc"
}

if [ -z "$(ls /var/www/html)" ]; then
    echo "Initializing leed sources"
    cd ..
    rmdir html
    git clone https://github.com/LeedRSS/Leed html
    cd html
    echo "Waiting for mysql installation"
    sleep 10
    echo "Preconfiguring leed"
fi
perms
git fetch
if [ "$MODE" == "prod" ]; then
    echo "Switching sources to $VERSION"
    git switch -d $VERSION
fi
rm -rf .cache/*

perms
php /install.php
sleep 1 && info &
exec apache2ctl -DFOREGROUND
