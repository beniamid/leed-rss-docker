<?php

chdir('/var/www/html');
require_once('Functions.class.php');
require_once('Install.class.php');
require_once('i18n.php');
global $i18n;
$install = new Install();
$installDirectory = dirname(__FILE__).'/install';

// N'affiche que les langues du navigateur
// @TODO: il faut afficher toutes les langues disponibles
//        avec le choix par défaut de la langue préférée
$languageList = Translation::getHttpAcceptLanguages();
if (!empty($lang)) {
    // L'utilisateur a choisi une langue, qu'on incorpore dans la liste
    array_unshift($languageList, $lang);
    $liste = array_unique($languageList);
}
unset($i18n); //@TODO: gérer un singleton et le choix de langue / liste de langue
$currentLanguage = i18n_init($languageList, $installDirectory);

$languageList = array_unique($i18n->languages);


if (file_exists('constant.php')) {
    die(_t('ALREADY_INSTALLED'));
}




$installActionName = 'installButton';
$_ = [
    'login' => $_ENV['INITIAL_USER'],
    'password' => $_ENV['INITIAL_PASSWORD'],
    'mysqlBase' => $_ENV['MARIADB_DATABASE'],
    'mysqlHost' => 'mariadb',
    'mysqlLogin' => $_ENV['MARIADB_USER'],
    'mysqlMdp' => $_ENV['MARIADB_PASSWORD'],
    $installActionName => true
];
$_SESSION=[];
$ret = $install->launch($_, $installActionName);
//echo JSON_ENCODE($install)."\n";
//echo "Done $ret\n";
